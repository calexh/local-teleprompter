# quick-teleprompter
quick-teleprompter is a dependency-free, lightweight, and easy to use package, consisting of an HTML file with options allowing one to set a teleprompter up on the local machine.

Command Line Instruction:
1. npx quick-teleprompter 
2. Choose your text-advancement settings in the browser window
3. Copy in your text
4. Click "Run Teleprompter"

Programmatic Usage (N/A):
1. npm i quick-teleprompter
2. Navigate to module directory
3. Run "node opener.js" or open index.html in web browser
4. Choose your text-advancement settings in the browser window
5. Copy in your text
6. Click "Run Teleprompter"

Features:
- Lightmode/Darkmode Toggle
- Tap to Advance Text Toggle
- Arrow Keys to Advance Text Toggle
- Scroll to Advance Text Toggle 
- Space bar to Advance Text Toggle 
- Time-based Text Autoadvance Speed Adjustment (25-250 words per minute) 
- Time-based Text Autoadvance Toggle 
- Restart Script Button
- Play / Pause Button
- Step Back Button

Git Repo: https://gitlab.com/calexh/local-teleprompter

Thank You for Using quick-teleprompter, written by C. Alex @ YHY/SPC LLC 

We Contract Internationally!  🇷🇺 - 🇲🇽 - 🇨🇿 - 🇫🇮 - 🇸🇪 - 🇩🇪 - 🇺🇸 

Visit https://YHY.fi & work with YHY/SPC LLC on your next project.