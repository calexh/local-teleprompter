#!/usr/bin/env node
// open index.html (for npx only)
var url = "\\index.html";
var start = (process.platform == 'darwin'? 'open': process.platform == 'win32'? 'start': 'xdg-open');
require('child_process').exec(start + ' ' +  __dirname + url);